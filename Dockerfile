FROM openjdk:8 as builder
ADD . /code/
RUN \
    cd /code/ && \
    rm -Rf target node_modules && \
    chmod +x /code/mvnw && \
    sleep 1 && \
    ./mvnw clean package -Pdev -DskipTests && \
    mv /code/target/*.war /coot-registry.war && \
    rm -Rf /code/ /root/.m2 /root/.cache /tmp/*

FROM openjdk:8-jre-alpine
ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS \
    JAVA_OPTS="" \
    SPRING_PROFILES_ACTIVE=dev,native \
    GIT_URI=https://gitlab.com/mouad.tk/jhipster-registry/ \
    GIT_SEARCH_PATHS=./central-config

EXPOSE 8761
RUN mkdir /target && \
    chmod g+rwx /target
CMD java \
        ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom \
        -jar /coot-registry.war \
        --spring.profiles.active=dev \
        --security.user.password=admin \
        --security.user.name=admin \
        --jhipster.security.authentication.jwt.secret=@Synov@FRcootSecret@Key2017!@

COPY --from=builder /coot-registry.war .
