package io.github.jhipster.registry.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String PARENT = "ROLE_PARENT";

    public static final String ENFANT = "ROLE_ENFANT";

    public static final String TIERE = "ROLE_TIERE";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {
    }
}
